package dev.ep2.gfx;

import java.awt.image.BufferedImage;

public class Assets {

	public static BufferedImage cabecaComum, corpoComum, 
	cabecaKitty, corpoKitty, cabecaStar, corpoStar, simpleFruit, 
	bombFruit, bigFruit, decreaseFruit, obstacle, planoFundo, telaInicial, nova, telaGameOver, telaFundoGameOver; 
	
	public static void init() {
		cabecaComum = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/cabecaComum.png");
		corpoComum = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/corpoComum.png");
		cabecaKitty = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/cabecaKitty.png");
		corpoKitty = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/corpoKitty.png");
		cabecaStar = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/cabecaStar.png");
		corpoStar = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/corpoStar.png");
		bigFruit = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/bigFruit.png");
		simpleFruit = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/simpleFruit.png");
		bombFruit = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/bombFruit.png");
		decreaseFruit= ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/decreaseFruit.png");
		obstacle = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/obstacle.png");
		planoFundo = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/planoDeFundo.png");
		telaInicial = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/telaInicial.png");
		telaGameOver = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/gameOver.png");
		telaFundoGameOver = ImageLoader.loadImage("/home/carlos/git/ep2/res/textures/telaFundoGameOver.png");
	}
	
}
