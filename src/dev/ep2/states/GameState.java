package dev.ep2.states;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;
import javax.swing.JFrame;
import dev.ep2.Obstacle;
import dev.ep2.Player;
import dev.ep2.gfx.Assets;

public class GameState implements KeyListener{

	private static final int width = 400;
	private static final int height = 400;
	private static final int COMUM = 1;
	private static final int KITTY = 2;
	private static final int STAR = 3;
	private static final int NaoEscolheuCobraAinda = 0;
	private static final int simpleFruit = 0;
	private static final int bombFruit = 1;
	private static final int bigFruit = 2;
	private static final int decreaseFruit = 3;
	private static final int numObstacle = 4;
	
	private boolean comeuFruta;
	private boolean left = false, right = false, up = false, down = false;
	private boolean jaEscolheuCobra = false;
	private boolean gameOver=false;
	
	private int tipoCobra;
	private int pontos = 3;
	private int frutaX = new Random().nextInt(400-10), frutaY = new Random().nextInt(400-10);
	private int score=0;
	private int tipoFruta = 0;
	private long tempoDaFruta;
	
	private Obstacle[] obstacle = new Obstacle[4];
	public Player[] player = new Player[width*height];
	
	
	public GameState() {
		//Cria cobrinha
		for(int i=0; i < pontos; i++) {
			player[i] = new Player(40 - i*10, 10);
		}
		//Cria obstacle
		for(int i=0; i<numObstacle; i++) {
			obstacle[i] = new Obstacle(new Random().nextInt(400-10), new Random().nextInt(400-10));
		}
	}

	public void tick() {
		
		verificaColisao();
		
		verificaSeChegouNaBorda();
		
		moveCobrinha();
		
		verificaSeComeuFruta();
		
		
	}
	
	public void verificaColisao() {
		
		for(int i=0; i<numObstacle; i++) {
			if(new Rectangle(player[0].x, player[0].y, 10, 10).intersects(new Rectangle(obstacle[i].x,obstacle[i].y,10,10)) && tipoCobra!=KITTY) {
				gameOver = true;
			}
		}
		for(int i = pontos-1; i>0; i--) {
			if(new Rectangle(player[0].x, player[0].y, 10, 10).intersects(new Rectangle(player[i].x,player[i].y,10,10))) {
				gameOver = true;
			}
		}
	}
	
	public void verificaSeChegouNaBorda() {
		if(player[0].x>=width) {
			player[0].x=0;
		}
		else if(player[0].x<0) {
			player[0].x=width;
		}
		else if(player[0].y>=height) {
			player[0].y=0;
		}
		else if(player[0].y<0) {
			player[0].y=height;
		}
	}

	
	public void moveCobrinha() {
		if(right!=false || left!=false || up!=false || down!=false) {
			for(int i = pontos - 1; i>0; i--) {
					player[i].x = player[i-1].x;
					player[i].y = player[i-1].y;
			}		
		}
		if(right) {
			player[0].x+=10;
		}
		else if(left) {
			player[0].x-=10;
		}
		else if(up) {
			player[0].y-=10;
		}
		else if(down) {
			player[0].y+=10;
		}
	}
	
	public void verificaSeComeuFruta() {
		comeuFruta=false;
		if(new Rectangle(player[0].x, player[0].y, 10, 10).intersects(new Rectangle(frutaX,frutaY,10,10))) {
			frutaX = new Random().nextInt(400-10);
			frutaY = new Random().nextInt(400-10);
			tempoDaFruta=0;
			if(tipoFruta==bombFruit) {
				gameOver=true;
				
			}
			else if(tipoFruta==simpleFruit) {
				score++;
				pontos++;
				player[pontos-1]=new Player(player[pontos-2].x,player[pontos-2].y);
			}
		
			else if(tipoFruta==bigFruit) {
				score+=2;
				pontos++;
				player[pontos-1]=new Player(player[pontos-2].x,player[pontos-2].y);
				
			}
			else if(tipoFruta==decreaseFruit) {
				pontos=3;
			}
			comeuFruta=true;
		}
		if(comeuFruta) {
			tipoFruta = new Random().nextInt(4-0);
		}
		
	}

	public void render(Graphics g, JFrame frame) {
		if(tipoCobra==NaoEscolheuCobraAinda) {
			g.drawImage(Assets.telaInicial, 0, 0, width, height, null);
		}
		if(tipoCobra!=NaoEscolheuCobraAinda&&gameOver==false) {
			jaEscolheuCobra = true;
			}
		if(jaEscolheuCobra&&gameOver==false) {
			desenhaCobra(g);
			
			desenhaFruta(g);
			
			desenhaObstacle(g);
			
			desenhaScore(g, frame);
		}
		if(gameOver) {
			desenhaFimJogo(g);
			desenhaScore(g, frame);
		}

	}
	
	public void desenhaCobra(Graphics g) {
		g.drawImage(Assets.planoFundo, 0, 0, width, height, null);
		if(tipoCobra==COMUM) {
			for(int i=0; i<pontos; i++) {
				if(i==0) 
					g.drawImage(Assets.cabecaComum, player[i].x, player[i].y, 10, 10, null);
				if(i!=0)
					g.drawImage(Assets.corpoComum, player[i].x, player[i].y, 10, 10, null);
			}
		}
		if(tipoCobra==KITTY) {
			for(int i=0; i<pontos; i++) {
				if(i==0) 
					g.drawImage(Assets.cabecaKitty, player[i].x, player[i].y, 10, 10, null);
				if(i!=0)
					g.drawImage(Assets.corpoKitty, player[i].x, player[i].y, 10, 10, null);
			}
		}
		if(tipoCobra==STAR) {
			for(int i=0; i<pontos; i++) {
				if(i==0) 
					g.drawImage(Assets.cabecaStar, player[i].x, player[i].y, 10, 10, null);
				if(i>0)
					g.drawImage(Assets.corpoStar, player[i].x, player[i].y, 10, 10, null);
			}
		}
	}
	
	public void desenhaFruta(Graphics g) {
		tempoDaFruta++;
		if(tipoFruta==simpleFruit) {
			g.drawImage(Assets.simpleFruit, frutaX, frutaY, 10, 10, null);
		}
		else if(tipoFruta==bombFruit) {
			g.drawImage(Assets.bombFruit, frutaX, frutaY, 10, 10, null);
		}
		else if(tipoFruta==bigFruit) {
			g.drawImage(Assets.bigFruit, frutaX, frutaY, 10, 10, null);
		}
		else if(tipoFruta==decreaseFruit) {
			g.drawImage(Assets.decreaseFruit, frutaX, frutaY, 10, 10, null);
		}
		
		if(tempoDaFruta==6000) {
			tipoFruta = new Random().nextInt(4-0);
			frutaX = new Random().nextInt(400-10);
			frutaY = new Random().nextInt(400-10);
			tempoDaFruta=0;
		}
	}
	
	public void desenhaObstacle(Graphics g) {
		
		for(int i = 0; i < numObstacle; i++) {
			g.drawImage(Assets.obstacle, obstacle[i].x, obstacle[i].y, 10, 10, null);
		}
		
	}
	
	public void desenhaScore(Graphics g, JFrame frame) {
		String Score = "Score " + score*100;
		Font SCORE_FONT = new Font("Consolas", Font.BOLD, 16);
		FontMetrics SCORE_METRICA = frame.getFontMetrics(SCORE_FONT);
		SCORE_METRICA = frame.getFontMetrics(SCORE_FONT);
		g.setColor(Color.white);
		g.setFont(SCORE_FONT);
		g.drawString(Score,(width-SCORE_METRICA.stringWidth(Score))-10,height-10);
	}
	
	public void desenhaFimJogo(Graphics g) {
		g.drawImage(Assets.telaFundoGameOver, 0, 0, width, height, null);
		g.drawImage(Assets.telaGameOver, 100, 150, null);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(jaEscolheuCobra==true&&gameOver==false) {
			if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
				right = true;
				up = false;
				down = false;
				left = false;
			}
			else if(e.getKeyCode() == KeyEvent.VK_LEFT) {
				right = false;
				up = false;
				down = false;
				left = true;;
			}
			else if(e.getKeyCode() == KeyEvent.VK_UP) {
				right = false;
				up = true;
				down = false;
				left = false;
			}
			else if(e.getKeyCode() == KeyEvent.VK_DOWN) {
				right = false;
				up = false;
				down = true;
				left = false;
			}
		}
		else if(jaEscolheuCobra==false&&gameOver==false) {
			if(e.getKeyCode()== KeyEvent.VK_C) {
				tipoCobra=1;
			}
			else if(e.getKeyCode()== KeyEvent.VK_K) {
				tipoCobra=2;
			}
			else if(e.getKeyCode()== KeyEvent.VK_S) {
				tipoCobra=3;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
			
	}

	@Override
	public void keyTyped(KeyEvent e) {
	
	}
	
}
	

