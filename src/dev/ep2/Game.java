package dev.ep2;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

 import dev.ep2.display.Display;
 import dev.ep2.gfx.Assets;
 import dev.ep2.states.GameState;


public class Game implements Runnable{

	 private Display display;
	 public int width, height;
	 public String title;
	 	 
	 public boolean running = false;
	 
	 private Thread thread;
	 
	 private BufferStrategy bs;
	 private Graphics g;
	 
	 //States
	 private GameState gameState;
	
	 
	 public Game(String title, int width, int height) {
		 this.width = width;
		 this.height = height;
		 this.title = title;
		 
	 } 
	 
	 private void init() {
		 display = new Display(title, width, height);
		 Assets.init();
		 gameState = new GameState();
		 display.getFrame().addKeyListener(gameState);
		
	 }
	 
	 private void tick() {
		 
		 gameState.tick();
	 }
	 
	 private void render() {
		 bs = display.getCanvas().getBufferStrategy();
		 if(bs == null) {
			 display.getCanvas().createBufferStrategy(3);
			 return;
		 }
		 g = bs.getDrawGraphics();
		 //Tela limpa
		 g.clearRect(0, 0, width, height);
		 
		 //Desenhar aqui
		 gameState.render(g, display.getFrame());
		
		 //Fim desenho
		 bs.show();
		 g.dispose();
	 }
	 
	 
	 public void run() {
		 
		 init();
		 
		 int fpsRender = 1500;
		 int fpsTick = 17;
		 double timePerTickRender = 1000000000 / fpsRender;
		 double timePerTickTick = 1000000000 / fpsTick;
		 
		 double deltaRender = 0;
		 double deltaTick = 0;
		 long now;
		 long lastTime = System.nanoTime();
		 long timer = 0;
		 long ticksTick = 0;
		 long ticksRender = 0;
		 
		 while(running) {
			 now = System.nanoTime();
			 deltaRender += (now-lastTime)/timePerTickRender;
			 deltaTick += (now-lastTime)/timePerTickTick;
			 timer += now - lastTime;
			 lastTime = now;
			 
			 if(deltaTick >= 1) {
				 tick();
				 ticksTick++;
				 deltaTick--;
			 }
			 if(deltaRender >= 1) {
				 render();
				 ticksRender++;
				 deltaRender--;
			 }
			 
			 if(timer >= 1000000000) {
				 // para conferir a quantos fps por segundo está rodando
				 // System.out.println("Fps tick e render: " + ticksTick + " " + ticksRender);
				 ticksTick = 0;
				 ticksRender = 0;
				 timer = 0;
			 }
		 }
		 
		 stop();
	 }
	 
	 public synchronized void start() {
		 if(running)
			 return;
		 running = true;
		 thread = new Thread(this);
		 thread.start();
	 }
	 
	 public synchronized void stop() {
		 if(running)
			 return;
		 running = false;
		 try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	 }
	
}
